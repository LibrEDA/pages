<!DOCTYPE html>

<!--
 Copyright (c) 2020-2021 Thomas Kramer.

 This file is part of librEDA 
 (see https://codeberg.org/libreda).

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
-->

<html>
<head>
    <title>LibrEDA</title>
    <meta charset="utf-8" />
    <meta name="description" content="Official website of the LibrEDA project." />
    <meta name="keywords" content="libreda, libre, open-source, eda, electronic, vlsi, libre EDA, FOSS EDA, framework, chip design, silicon, rust, python" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    
    <meta property="og:type" content="website" />
    <meta property="og:title" content="LibrEDA - Libre Chip Design Framework" />
    <meta property="og:site_name" content="LibrEDA.org" />
    <meta property="og:image" content="https://libreda.org/img/libreda_logo_square_128x128.png" /> 
    <meta property="og:description" content="Libre Electronic Design Automation Project." />

    <link rel="stylesheet" type="text/css" href="style.css" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
</head>
<body>

    <div id="content" class="flexvertical">

        <h1><img src="img/libreda_logo_square.svg" height="40px" style="margin-bottom: -5px"/> LibrEDA 🕊️</h1>

        <p>
        <i>Libre Electronic Design Automation</i>
        </p>

        <div class="box" >
            <p>
            LibrEDA is a young libre-software framework for the physical design of silicon chips and tries to simplify the development of place and route tools. A strong motivation is democratization of silicon technology by making ASIC toolchains easily accessible for research, education and hobbyists.

            </p>
            <p>
            Today's society and especially the internet stand on integrated electronic circuits. Practically all chips that run the internet today are proprietary and closed-source pieces of hardware. This is a problem for transparency, trust, security and digital sovereignty. To build open-source alternatives it is important to have open-source Electronic Design Automation (EDA) software. Since the underlying optimization problems are not only computationally complex but also complicated it is necessary to create a collaborative ecosystem. The framework approach of LibrEDA is an attempt to go into this direction.

            </p>

            <p>
            Links:
            <ul>
                <li><a href="https://codeberg.org/LibrEDA/LibrEDA">Main Page</a></li>
                <li><a href="https://codeberg.org/LibrEDA/libreda-rs-workspace">Code</a></li>
                <li><a href="https://codeberg.org/LibrEDA/libreda-examples">Examples</a></li>
                <li><a href="https://nlnet.nl/project/ChipDesignFramework/">NLNet project page</a></li>
            </ul>

            Side projects:
            <ul>
                <li><a href="https://codeberg.org/librecell">LibreCell - Standard-cell generator and characterization tool</a></li>
            </ul>
            </p>

            <p>
                <center><h2>Documentation</h2></center>
                <ul>
                    <li><a href="./book/index.html">LibrEDA Book</a></li>
                    <li><a href="./doc/libreda_db/index.html">libreda-db</a></li>
                    <li><a href="./doc/libreda_pnr/index.html">libreda-pnr</a></li>
                </ul>
            </p>


            <p>
                <center><h2>Talks</h2></center>
                <ul>
                    <li><a href="https://peertube.f-si.org/videos/watch/1d664376-7de1-481c-a4ee-e9377287292b">LibrEDA @ FSiC2022</a></li>
                </ul>
            </p>

            <p>
            <center>
                <h2>Components</h2>
            </center>
                The Rust software framework currently features most basic components. This includes input/output for common file formats (OASIS, LEF/DEF, ...) as well as example implementations of algorithms for standard-cell placement, legalization and routing. <br />
            <center>
                <a href="https://codeberg.org/LibrEDA/iron-shapes"><img src="./img/libreda_shapes_logo_square.svg" title="shapes: geometric primitives" width="64px" /></a>
                <a href="https://codeberg.org/LibrEDA/libreda-db"><img src="./img/libreda_db_logo_square.svg" title="libreda-db: Data structures for layouts and netlists." width="64px" /></a>
                <a href="https://codeberg.org/LibrEDA/libreda-oasis"><img src="./img/libreda_oasis_logo_square.svg" title="libreda-oasis: Layout I/O in the OASIS format." width="64px" /></a>
                <a href="https://codeberg.org/LibrEDA/electron-placer"><img src="./img/libreda_electron_logo_square.svg" title="electron-placer: Example standard-cell placer." width="64px" /></a>
                <a href="https://codeberg.org/LibrEDA/tetris-legalizer"><img src="./img/libreda_tetris_logo_square.svg" title="tetris-legalizer: Example standard-cell legalizer." width="64px" /></a>
                <a href="https://codeberg.org/LibrEDA/mycelium-router"><img src="./img/libreda_mycelium_logo_square.svg" title="mycelium-router: Example routing algorithm." width="64px" /></a>
            </center>
            </p>

            <center>
                <h2>Example Output</h2>
            </center>
            Illustration of a simple digital chip layout created with the framework. The most visible parts are the wires that connect the standard-cells.
            <center>
                <img src="./img/place_and_route_illustration.png" title="Illustration of placed and routed design with 1600 cells. FreePDK45." width="256px" />
            </center>

        </div>

        <div id="acknowledgements" class="box">

            <p>
            This project was funded by <a href="https://nlnet.nl/project/ChipDesignFramework/">NLNet and NGI0</a>
            </p>

            <p>
            <center>
                <a href="https://NLnet.nl"><img src="./img/nlnet.nl_banner.svg" alt="Logo NLnet: abstract logo of four people seen from above" height="64" /></a>
                <a href="https://NLnet.nl/NGI0"><img src="./img/NGI0_tag.svg" alt="Logo NGI Zero: letterlogo shaped like a tag" height="64" /></a>
            </center>
            </p>

            <p>
            This project was funded through the <a href="https://NLnet.nl/PET">NGI0 PET</a> Fund,
            a fund established by NLnet with financial support from the European
            Commission's <a href="https://ngi.eu">Next Generation Internet</a>
            programme, under the aegis of DG Communications Networks, Content and
            Technology under grant agreement No 825310.
            </p>
        </div>

    </div>

    <div id="footer">
        <a href="https://codeberg.org/librEDA/pages">Source Code</a>
        <a href="./imprint.html">Imprint/Impressum</a>
    </div>

</body>
</html>
